//
//  ArticleCell.swift
//  Mandiri_VIPER
//
//  Created by Nanda Mochammad on 20/07/21.
//

import UIKit
import SDWebImage

class ArticleCell: UITableViewCell{
    
    @IBOutlet weak var titleCell: UILabel!
    @IBOutlet weak var descriptionCell: UILabel!
    @IBOutlet weak var imageCell: UIImageView!
    
    func setCel(data: DataArticle) {
        titleCell.text = data.title
        descriptionCell.text = data.description
        imageCell.image = UIImage(named: "imgDefault")

        guard let img = data.urlToImage, let url = URL(string: img) else { return }
        imageCell.sd_setImage(with: url, completed: nil)
    }
}
