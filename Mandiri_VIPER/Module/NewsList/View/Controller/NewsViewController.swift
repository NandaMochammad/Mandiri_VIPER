//
//  NewsViewController.swift
//  Mandiri_VIPER
//
//  Created by Nanda Mochammad on 20/07/21.
//

import UIKit

class NewsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    var presenter: NewsListViewToPresenterProtocol?
    
    // MARK: - Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
        presenter?.updateView()
        tableView.register(UINib(nibName: "ArticleCell", bundle: .main), forCellReuseIdentifier: "ArticleCell")

    }
}
// MARK: - UITableViewDataSource
extension NewsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.getNewsListCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath) as! ArticleCell
        let row = indexPath.row
        if let news = presenter?.getNews(index: row) {
            cell.setCel(data: news)
            return cell
        } else {
            return cell
        }
    }
}

// MARK: - UITableViewDelegate
extension NewsViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height )){
            self.presenter?.updateView()
        }
   }
}

// MARK: - LiveNewsListPresenterToViewProtocol
extension NewsViewController: NewsListPresenterToViewProtocol {

    func showNews() {
        tableView.reloadData()
    }
    
    func showError() {
        self.presentAlertPerhatian(message: "Mohon Maaf Terjadi Gangguan, Silakan coba kembali sesaat lagi")
    }
}
