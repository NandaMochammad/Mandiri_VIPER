//
//  NewsInteractor.swift
//  Mandiri_VIPER
//
//  Created by Nanda Mochammad on 20/07/21.
//

import Foundation

import Alamofire

class NewsInteractor: NewsListPresentorToInteractorProtocol {

    // MARK: - Properties
    weak var presenter: NewsListInteractorToPresenterProtocol?
    var news: [DataArticle]? {
        didSet {
            if let news = news {
                currentArticleTotal = news.count
            }
        }
    }
    
    var articlePage = 0
    var currentArticleTotal = 0
    var totalArticleAvailable = 0

    // MARK: - Methods
    func fetchNews() {
        articlePage = pageToLoad()
        GetArticleRequest.requestSource(cat: "", domain: "", page: articlePage) { isDone, res, err in
            if isDone {
                if let resource = res {
                    if self.news != nil {
                        self.news?.append(contentsOf: resource.articles)
                    } else {
                        self.news = resource.articles
                    }
                    
                    self.totalArticleAvailable = resource.totalResults
                    self.presenter?.newsFetched()
                }else {
                    guard let err =  err else { return }
                    print(err)
                }
            } else {
                self.presenter?.newsFetchedFailed()
            }
        }
    }
    
    func removeArticleData() {
        articlePage = 0
        currentArticleTotal = 0
        totalArticleAvailable = 0
    }
    
    func pageToLoad() -> Int {
        if currentArticleTotal != 0 {
            if currentArticleTotal < 20 {
                articlePage += 1
                return articlePage
            } else {
                return 0
            }
        } else if currentArticleTotal == 0{
            articlePage = 1
            return articlePage
        } else {
            return 0
        }
    }
}
