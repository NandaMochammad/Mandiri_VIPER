//
//  NewsPresenter.swift
//  Mandiri_VIPER
//
//  Created by Nanda Mochammad on 20/07/21.
//

import Foundation

class NewsPresenter: NewsListViewToPresenterProtocol {

    
    
    // MARK: - Properties
    weak var view: NewsListPresenterToViewProtocol?
    var interactor: NewsListPresentorToInteractorProtocol?
    var router: NewsListPresenterToRouterProtocol?
    
    // MARK: - Methods
    func updateView() {
        interactor?.fetchNews()
    }
    
    func getNewsListCount() -> Int? {
        return interactor?.news?.count
    }
    
    func getNews(index: Int) -> DataArticle? {
        return interactor?.news?[index]
    }
}

// MARK: - LiveNewsListInteractorToPresenterProtocol
extension NewsPresenter: NewsListInteractorToPresenterProtocol {
    
    func newsFetched() {
        view?.showNews()
    }
    
    func newsFetchedFailed() {
        view?.showError()
    }
}
