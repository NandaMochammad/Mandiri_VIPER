//
//  NewsProtocol.swift
//  Mandiri_VIPER
//
//  Created by Nanda Mochammad on 20/07/21.
//

import Foundation
import UIKit

protocol NewsListPresenterToViewProtocol: class {
    func showNews()
    func showError()
}

protocol NewsListInteractorToPresenterProtocol: class {
    func newsFetched()
    func newsFetchedFailed()
}

protocol NewsListPresentorToInteractorProtocol: class {
    var presenter: NewsListInteractorToPresenterProtocol? { get set }
    var news: [DataArticle]? { get }
    
    func fetchNews()
}

protocol NewsListViewToPresenterProtocol: class {
    var view: NewsListPresenterToViewProtocol? { get set }
    var interactor: NewsListPresentorToInteractorProtocol? { get set }
    var router: NewsListPresenterToRouterProtocol? { get set }
    
    func updateView()
    func getNewsListCount() -> Int?
    func getNews(index: Int) -> DataArticle?
}

protocol NewsListPresenterToRouterProtocol: class {
    static func createModule() -> UIViewController
}
