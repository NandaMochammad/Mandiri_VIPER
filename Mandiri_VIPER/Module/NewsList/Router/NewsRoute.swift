//
//  NewsRoute.swift
//  Mandiri_VIPER
//
//  Created by Nanda Mochammad on 20/07/21.
//

import Foundation
import UIKit

class NewsRouter: NewsListPresenterToRouterProtocol{
    
    class func createModule() -> UIViewController {
        
        let view = NewsViewController()
        let presenter: NewsListViewToPresenterProtocol & NewsListInteractorToPresenterProtocol = NewsPresenter()
        let interactor: NewsListPresentorToInteractorProtocol = NewsInteractor()
        let router: NewsListPresenterToRouterProtocol = NewsRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
}
