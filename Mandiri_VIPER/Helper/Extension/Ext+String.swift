//
//  Ext+String.swift
//  Mandiri_VIPER
//
//  Created by Nanda Mochammad on 20/07/21.
//

import Foundation
import Alamofire

extension String: ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
    
    func replaceSpaceInURL() -> String{
        let str = self
        let newStr : String = str.replacingOccurrences(of: " ", with: "%20")
        return newStr
    }
}
