//
//  WebserviceRequest.swift
//  Mandiri_VIPER
//
//  Created by Nanda Mochammad on 20/07/21.
//

import Foundation
import Alamofire

struct WebserviceResource<T: Codable>{
    let url: URL
    var parameters: [String: Any]
    var headers: HTTPHeaders
    var encoding: String
    
    init(url: URL, params: [String: Any]? = [:], headers: HTTPHeaders? = [:], encoding: String? = "") {
        self.url = url
        self.parameters = params!
        self.headers = headers!
        self.encoding = encoding ?? ""
    }
}
