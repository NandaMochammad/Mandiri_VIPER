//
//  Webservice.swift
//  Mandiri_VIPER
//
//  Created by Nanda Mochammad on 20/07/21.
//

import Foundation
import Alamofire
import SwiftyJSON

public enum NetworkError: Error{
    case decodingError
    case domainError
    case urlError
    case noConnection
}


class Webservice{
    func requestGET<T>(resource: WebserviceResource<T>, completion: @escaping(Result<T>) -> Void){
        Alamofire.request(resource.url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: resource.headers)
            .validate()
            .responseData { (responseData) in
                print("Request: \(resource.url)")
                switch responseData.result{
                case .success(let data):
                    do{
                        let result = try JSONDecoder().decode(T.self, from: data)
                        print("Data GET, ", JSON(responseData.data!.count))
                        completion(.success(result))
                    }catch{
                        print("Error Decoding, ", error.localizedDescription)
                        print("OUTPUT JSON: ", JSON(data))
                        completion(.failure(NetworkError.decodingError))
                    }
                case .failure(let err):
                    GlobalData.shared.jsonFailure = JSON(responseData.data!)
                    print(JSON(responseData.data!))
                    print("Error Networking, ", err.localizedDescription)
                    completion(.failure(NetworkError.domainError))
                }
        }
    }
}


final class GlobalData{
    static let shared = GlobalData()
    
    var jsonFailure: JSON?
    
    private init(){}
}
