//
//  Constant.swift
//  Mandiri_VIPER
//
//  Created by Nanda Mochammad on 20/07/21.
//

import Foundation

struct Constant {
    let api: String = "daf1a049bda94a8bb510f0f2f5708c51"
    
    func articleURL(category: String, domain: String, page: Int)-> String {
        return "https://newsapi.org/v2/everything?domains=bloomberg.com&pageSize=5&page=\(page)&apiKey=\(api)&language=en"
    }
}
