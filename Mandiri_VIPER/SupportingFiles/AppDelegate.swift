//
//  AppDelegate.swift
//  Mandiri_VIPER
//
//  Created by Nanda Mochammad on 20/07/21.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let news = NewsRouter.createModule()
                
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = news
        window?.makeKeyAndVisible()

        return true
    }

}

